from flask import Flask, render_template,url_for, request, redirect,flash, session
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_mail import Mail

import os 
import psycopg2


app = Flask(__name__)
app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
mail = Mail(app)

from send_mail import  mail_validation, valid_code
from models import User, Patient_info, Doctor_info
from email_and_phone import chk_mail_and_ph, confirm_token
from otp import otp_confirm


@app.route('/', methods = ['GET', 'POST'])
def home():
    return render_template('home.html')


@app.route('/login', methods = ['GET', 'POST'])
def login():

    if request.method == "POST":
        psw = request.form['psw']
        user = User.query.filter_by(email = request.form['email']).first()
        
        if user and bcrypt.check_password_hash( user.password, psw):    

            if user.confirmed:
                session['logged_in'] = True
                valid_code(user.email)
                return redirect("/login/welcome")
            
            else:
                flash('Your email is not validated')

        else:
            flash('Your credentials were not matched') 
        
    return render_template('login.html')


@app.route('/patient_registration', methods = ['GET', 'POST'])
def patient_registration():
    
    if request.method == "POST": 
        email =  request.form['email']  
        data = chk_mail_and_ph(
            "patient",
            request.form['first_name'],
            request.form['last_name'],
            request.form['phone'],
            email,    
            request.form['psw']
            )

        if not data:

            return redirect("/patient_registration")
        else:
            
            mail_validation(email)

            db.session.add(data)
            p_data = Patient_info(
                request.form['problem'],
                request.form['description'],
                patient=[data]
                )
            db.session.add( p_data )
            db.session.commit()
            return redirect("/validation")
                
    return render_template('patient_registration.html')

@app.route('/doctor_registration', methods = ['GET', 'POST'])
def signup_for_doctor():
    if request.method == "POST":
        email = request.form["email"]
        data = chk_mail_and_ph(
            "Doctor",
            request.form['first_name'],
            request.form['last_name'],
            request.form['phone'],
            email,    
            request.form['psw']
            )

        if not data:
            return redirect("/doctor_registration")
        else:
            
            mail_validation(email)

            db.session.add( data )
            db.session.add( Doctor_info(request.form['specialist'],doctor=[data]))
            db.session.commit()
            return redirect("/validation")

    return render_template('doctor_registration.html')

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash("Logged out Successfully!!!")
    return redirect('/')


@app.route('/login/welcome', methods = ['GET', 'POST'])
def welcome():
    if request.method == "POST":
        if otp_confirm(request.form["otp"]):
            return redirect("/profile")
        else:
            flash("Entered code is invalid")
    return render_template("welcome.html")


@app.route('/validation', methods = ['GET', 'POST'])
def validation():
    return render_template("validation.html")


@app.route("/validation/<token>")
def confirm_email(token):
    
    try:
        email= confirm_token(token)
    except:
        flash("The confirmation link has expured or invalid", "danger")
    
    user = User.query.filter_by(email=email).first_or_404()
    
    if  user.confirmed:
        flash('Your account has already been confirmed')
    else:
        user.confirmed = True
        db.session.add(user)
        db.session.commit()
        flash( "COngrats!! YOur account has been validated and now log in", "success" )
    return redirect('/login')
    
@app.route("/profile")
def profile():
    return "This is your profile"

app.run(debug = True)
