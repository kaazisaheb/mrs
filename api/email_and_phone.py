#checking for email and phone number repeat
#generating and confirm the registered token


from flask import flash
from models import User
from itsdangerous import URLSafeTimedSerializer
from app import app, db, bcrypt


def chk_mail_and_ph( job, firstname, lastname, phone, email, psw):

    if db.session.query(User).filter(User.email == email).count() == 0:

        if db.session.query(User).filter(User.phone == phone).count() == 0:
            password = bcrypt.generate_password_hash(psw).decode('utf-8')
            data = User(
                job,
                firstname,
                lastname,
                phone,
                email,
                password
            )

            return data

        else:
            flash('Phone Number already exists','error')
            return False

    else:
        flash('Email already exists','error')
        return False


def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    return serializer.dumps(email, salt=app.config['SECURITY_PASSWORD_SALT'])


def confirm_token(token, expiration=3600):
    serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    
    try:
        email = serializer.loads(
            token,
            salt=app.config['SECURITY_PASSWORD_SALT'],
            max_age=expiration
        )
    except:
        return False
    
    return email
