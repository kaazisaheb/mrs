from app import db

class User(db.Model):
    __tablename__ = "user_info"

    id = db.Column( db.Integer, primary_key= True )
    job = db.Column( db.String(10) )
    patient_id = db.Column( db.Integer, db.ForeignKey('patient.id') )
    doctor_id = db.Column( db.Integer, db.ForeignKey('doctor.id') )
    confirmed = db.Column(db.Boolean, nullable = False)
    first_name = db.Column( db.String(100), nullable=False )
    last_name = db.Column( db.String(100), nullable=False )
    phone = db.Column( db.Numeric(10), nullable=False ) 
    email = db.Column( db.String(100), nullable=False, unique=True )
    password = db.Column( db.String(64), nullable = False)
    
    
    def __init__(self, job, first_name, last_name, phone, email, password, confirmed =  False ):
        self.job = job
        self.first_name = first_name
        self.last_name = last_name
        self.phone = phone
        self.email = email
        self.password = password 
        self.confirmed = confirmed 
        
    def __repr__(self):
        return "<User_info(id='%s')>" % self.id

class Patient_info(db.Model):
    __tablename__ = "patient"

    id = db.Column( db.Integer, primary_key=True)
    problem = db.Column(db.String(100), nullable = False)
    problem_description = db.Column( db.Text, nullable = False)
    patient = db.relationship( 'User', backref="Patient_info" )

    def __init__(self, problem, problem_description, patient=[]):
        self.problem = problem
        self.problem_description = problem_description
        self.patient = patient

    def __repr__(self):
        return "<Patient_info(id='%s')>" % self.id

class Doctor_info( db.Model ):
    __tablename__ = "doctor"

    id = db.Column( db.Integer, primary_key=True)
    specialist = db.Column( db.String(100), nullable = False )
    doctor = db.relationship( 'User',  backref="Doctor_info" )

    def __init__( self, speciality, doctor=[] ):
        self.specialist = speciality
        self.doctor = doctor

    def __repr__( self ):
        return "<Doctor_info(id='%s')>" % self.id
