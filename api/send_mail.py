#sending mail for both registering and otp 
from flask import render_template, url_for, flash
from flask_mail import Message
from email_and_phone import  generate_confirmation_token
from app import app,mail
from otp import otp_generate

def send_email(to, subject, template ):
    msg = Message(
        subject,
        recipients=[to],
        html=template,
        sender=app.config['MAIL_DEFAULT_SENDER']
    )
    mail.send(msg)

def mail_validation(email):
    token = generate_confirmation_token( email )
    confirm_url = url_for('confirm_email', token=token, _external=True)
    html = render_template('email.html', confirm_url=confirm_url)
    subject = "Please confirm your email"
    send_email(email, subject, html)
    flash('A confirmation email has been sent via email.', 'success')

def valid_code(email):
    passcode= otp_generate()
    html = render_template("valid_email.html", passcode = passcode)
    subject = "Please confirm your log in validation"
    send_email(email,  subject, html)
    flash("A login code has been sent to your email")